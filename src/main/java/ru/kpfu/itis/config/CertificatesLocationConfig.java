package ru.kpfu.itis.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:certificates.properties")
@Getter
public class CertificatesLocationConfig {

    @Value("${certificates_of_employment_files_location}")
    String certificateOfEmploymentFilesLocation;
    @Value("${certificates_of_deductions_files_location}")
    String certificateOfDeductionsFilesLocation;
    @Value("${certificate_of_insurance_deductions_from_tax_service_files_location}")
    String certificateOfInsuranceDeductionsFromTaxServiceFilesLocation;

}

package ru.kpfu.itis.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:profile_images.properties")
@Getter
public class ImagesLocationConfig {

    @Value("${profile_images_files_location}")
    String profileImagesFilesLocation;

}

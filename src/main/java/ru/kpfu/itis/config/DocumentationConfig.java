package ru.kpfu.itis.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.io.IOException;
import java.util.ArrayList;

@Configuration
@EnableSwagger2
public class DocumentationConfig {


    private static final String securitySchemaOAuth2 = "oauth2";
    private static final String authorizationScopeGlobal = "global";
    private static final String authorizationScopeGlobalDesc = "accessEverything";


    private OAuthProperties oauthProperties;

    @Autowired
    DocumentationConfig(OAuthProperties oauthProperties) {
        this.oauthProperties = oauthProperties;
    }


    @Bean
    public Docket api() throws IOException {

        //Adding Header
        ParameterBuilder aParameterBuilder = new ParameterBuilder();
        aParameterBuilder.name("Authorization")                 // name of header
                .modelRef(new ModelRef("string"))
                .parameterType("header")               // type - header
                .defaultValue("Bearer em9uZTpteXBhc3N3b3Jk")        // based64 of - zone:mypassword
                .required(true)                // for compulsory
                .build();
        java.util.List<Parameter> aParameters = new ArrayList<>();
        aParameters.add(aParameterBuilder.build());             // add parameter
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage("ru.kpfu.itis.controller"))
                .paths(PathSelectors.any())
                .build().
                        pathMapping("")
                .globalOperationParameters(aParameters);
    }

}

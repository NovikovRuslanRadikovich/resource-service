package ru.kpfu.itis.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
@Getter
public class OAuthProperties {

    @Value("${security.oauth2.client.access-token-uri}")
    public String oauthAccessTokenUri;
    @Value("${security.oauth2.client.client-id}")
    String clientName;
    @Value("${security.oauth2.client.client-secret}")
    String clientSecret;

}

package ru.kpfu.itis.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.kpfu.itis.dto.ArchiveFilterDto;
import ru.kpfu.itis.dto.ListedGraduateDto;
import ru.kpfu.itis.service.ExcelService;
import ru.kpfu.itis.service.GraduateService;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/archive")
public class ArchiveController {

    private GraduateService graduateService;
    private ExcelService excelService;

    @Autowired
    ArchiveController(GraduateService graduateService, ExcelService excelService) {
        this.graduateService = graduateService;
        this.excelService = excelService;
    }

    @RequestMapping(value="/by_regions",method = RequestMethod.GET)
    public Map<Integer,Long> getGraduatesCountByRegions() {

        return graduateService.getGraduatesCountByRegions();
    }

    /**
     * Handle request to download an Excel document
     */
    @RequestMapping(value = "/export", method = RequestMethod.POST)
    public void export(HttpServletResponse response, @RequestBody ArchiveFilterDto archiveFilterDto) {
        try {
            response.setHeader("Content-disposition", "attachment; filename=test.xls");
            response.getOutputStream().write(excelService.graduatesToExcel(graduateService.getArchivedList(archiveFilterDto)));
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/filtered", method = RequestMethod.POST)
    public ResponseEntity<List<ListedGraduateDto>> getGraduatesByFilter(@RequestBody ArchiveFilterDto archiveFilterDto) {

        return ResponseEntity.ok(graduateService.getArchivedList(archiveFilterDto));
    }

}

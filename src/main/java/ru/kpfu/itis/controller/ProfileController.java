package ru.kpfu.itis.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.itis.dto.GraduateInfoDto;
import ru.kpfu.itis.dto.ProfileDto;
import ru.kpfu.itis.dto.WorkingPlaceDto;
import ru.kpfu.itis.dto.WorkingPlaceWithCertificatesInfoDto;
import ru.kpfu.itis.service.CertificateService;
import ru.kpfu.itis.service.ExcelService;
import ru.kpfu.itis.service.ProfileService;
import ru.kpfu.itis.service.WorkingPlaceService;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/profile")
public class ProfileController {

    private final DefaultTokenServices tokenServices;
    private ProfileService profileService;
    private ExcelService excelService;
    private WorkingPlaceService workingPlaceService;
    private CertificateService certificateService;

    @Autowired
    ProfileController(ProfileService profileService, DefaultTokenServices tokenServices, ExcelService excelService, WorkingPlaceService workingPlaceService, CertificateService certificateService) {
        this.profileService = profileService;
        this.tokenServices = tokenServices;
        this.excelService = excelService;
        this.workingPlaceService = workingPlaceService;
        this.certificateService = certificateService;
    }

    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public ResponseEntity<?> addProfile(@RequestBody ProfileDto profileDto, HttpServletRequest request) {

        Integer currentUserId = getUserIdFromRequest(request);
        profileService.fulfillProfile(profileDto, currentUserId);

        return ResponseEntity.ok("Profile Information has been stored");
    }

    @RequestMapping(value = "/certificates", method = RequestMethod.POST, consumes = {"multipart/form-data"})
    public ResponseEntity<?> addProfile(@RequestParam(value = "certificate_of_employment", required = false) MultipartFile certificateOfEmploymentFile,
                                        @RequestParam(value = "certificates_of_deductions", required = false) MultipartFile certificateOfDeductions,
                                        @RequestParam(value = "certificate_of_insurance_deductions_from_tax_service", required = false) MultipartFile certificateOfInsuranceDeductionsFromTaxService,
                                        HttpServletRequest request) {
        Integer currentUserId = getUserIdFromRequest(request);
        try {
            certificateService.saveCertificates(currentUserId,certificateOfEmploymentFile,certificateOfDeductions,certificateOfInsuranceDeductionsFromTaxService);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Couldn't save certificates about employment");
        }
        return ResponseEntity.ok("Certificates have been stored");
    }


    @RequestMapping(value = "/image", method = RequestMethod.POST, consumes = {"application/json", "multipart/form-data"})
    public ResponseEntity<?> loadPhotography(HttpServletRequest request, @RequestParam("photography") MultipartFile photoFile) {
        Integer currentUserId = getUserIdFromRequest(request);

        profileService.savePhoto(currentUserId, photoFile);

        return ResponseEntity.ok("Profile image has been stored");
    }

    @RequestMapping(value = "/image", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<?> getImage(HttpServletRequest request) {
        Integer currentUserId = getUserIdFromRequest(request);

        InputStreamResource resource = profileService.getImage(currentUserId);

        return ResponseEntity.ok().contentType(MediaType.parseMediaType("application/octet-stream")).body(resource);
    }

    @RequestMapping(value = "/historyOfJobApplications", method = RequestMethod.GET)
    public ResponseEntity<List<WorkingPlaceWithCertificatesInfoDto>> getHistoryOfJobApplications(HttpServletRequest request) {
        Integer currentUserId = getUserIdFromRequest(request);
        List<WorkingPlaceWithCertificatesInfoDto> workingPlaceWithCertificatesInfoDtoList = workingPlaceService.getWorkingPlaceWithCertificatesInfo(currentUserId);

        return ResponseEntity.ok(workingPlaceWithCertificatesInfoDtoList);
    }

    @RequestMapping(value = "/workingPlace", method = RequestMethod.POST)
    public ResponseEntity<?> createNewWorkingPlace(@RequestPart("workingPlace") WorkingPlaceDto
                                                           workingPlaceDto, HttpServletRequest request) {
        Integer currentUserId = getUserIdFromRequest(request);

        WorkingPlaceDto returnedWorkingPlaceDto = workingPlaceService.createWorkingPlace(currentUserId, workingPlaceDto);

        return ResponseEntity.ok().body("Working place for graduate is successfully created");
    }


    @RequestMapping(value = "/workingPlace", method = RequestMethod.PUT)
    public ResponseEntity<?> updateLastWorkingPlace(@RequestPart("workingPlace") WorkingPlaceDto
                                                           workingPlaceDto, HttpServletRequest request) {
        Integer currentUserId = getUserIdFromRequest(request);

        WorkingPlaceDto returnedWorkingPlaceDto = workingPlaceService.updateLastWorkingPlace(currentUserId, workingPlaceDto);

        return ResponseEntity.ok().body("Working place for graduate is successfully created");
    }

    @RequestMapping(value = "/profile", method = RequestMethod.PUT, consumes = "application/json")
    public ResponseEntity<?> updateProfile(@RequestBody GraduateInfoDto graduateInfoDto,
                                           HttpServletRequest request) {
        Integer currentUserId = getUserIdFromRequest(request);

        profileService.updateGraduateInfo(currentUserId, graduateInfoDto);
        return ResponseEntity.ok().body("Graduate Info has been updated");
    }



    @RequestMapping(value = "/pdf/{graduateId}/{fileName:.+}", method = RequestMethod.GET, produces = "application/pdf")
    public ResponseEntity<InputStreamResource> download(HttpServletRequest request, @PathVariable("graduateId") Integer graduateId, @PathVariable("fileName") String fileName) throws IOException {
        Integer currentUserId = getUserIdFromRequest(request);

        ClassPathResource pdfFile = new ClassPathResource("downloads/" + fileName);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.setContentLength(pdfFile.contentLength());
        return new ResponseEntity<>(new InputStreamResource(pdfFile.getInputStream()), headers, HttpStatus.OK);
    }

    private Integer getUserIdFromRequest(HttpServletRequest request) {
        String accessToken = request.getHeader("Authorization").split("\\s")[1];
        return (Integer) tokenServices.readAccessToken(accessToken).getAdditionalInformation().get("id");
    }

}
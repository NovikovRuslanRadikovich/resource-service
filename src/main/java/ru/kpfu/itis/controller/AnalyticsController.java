package ru.kpfu.itis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.kpfu.itis.dto.EmployerAndStudentsNumberDto;
import ru.kpfu.itis.dto.RegionAndStudentsNumberDto;
import ru.kpfu.itis.dto.TaxAmountAndYearDto;
import ru.kpfu.itis.service.GraduateService;
import ru.kpfu.itis.service.TaxService;
import ru.kpfu.itis.service.WorkingPlaceService;

import java.util.List;

@RestController
@RequestMapping("/analytics")
public class AnalyticsController {

    DefaultTokenServices tokenServices;
    WorkingPlaceService workingPlaceService;
    GraduateService graduateService;
    TaxService taxStatementService;

    @Autowired
    AnalyticsController(DefaultTokenServices tokenServices, WorkingPlaceService workingPlaceService, GraduateService graduateService, TaxService taxStatementService) {
        this.tokenServices = tokenServices;
        this.workingPlaceService = workingPlaceService;
        this.graduateService = graduateService;
        this.taxStatementService = taxStatementService;
    }

    @RequestMapping(value = "/employers", method = RequestMethod.GET)
    public ResponseEntity<List<EmployerAndStudentsNumberDto>> infoByEmployees() {

        return ResponseEntity.ok(workingPlaceService.getEmployersAndNumberOfStudents());
    }

    @RequestMapping(value = "/regions", method = RequestMethod.GET)
    public ResponseEntity<List<RegionAndStudentsNumberDto>> infoByRegionsOfComing() {

        return ResponseEntity.ok(graduateService.getRegionsAndNumbersOfStudents());

    }


    @RequestMapping(value = "/taxStatement", method = RequestMethod.GET)
    public ResponseEntity<TaxAmountAndYearDto> infoAboutGatheredTaxesByYears(@RequestParam("yearOfGraduation") Integer yearOfGraduation) {

        //ToDo check that token corresponds to admin
//        return ResponseEntity.ok(graduateService.getRegionsAndNumbersOfStudents());

        return ResponseEntity.ok(taxStatementService.getTaxAmountsByYear(yearOfGraduation));
    }


}

package ru.kpfu.itis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.kpfu.itis.dto.GraduateAndReturnedMoneyStatusDto;
import ru.kpfu.itis.dto.GraduatesFilterDto;
import ru.kpfu.itis.dto.ListedGraduateDto;
import ru.kpfu.itis.service.GraduateService;

import java.util.List;

@RestController
@RequestMapping("/graduates")
public class GraduateController {

    GraduateService graduateService;

    @Autowired
    GraduateController(GraduateService graduateService) {
        this.graduateService = graduateService;
    }

    @RequestMapping(value = "/filtered", method = RequestMethod.POST)
    public ResponseEntity<List<ListedGraduateDto>> getGraduatesByFilter(@RequestBody GraduatesFilterDto graduatesFilterDto) {

        return ResponseEntity.ok(graduateService.getGraduatesWithMissedCertificateFilter
                (graduatesFilterDto.getYearOfGraduation(),
                        graduatesFilterDto.getStatus(),
                        graduatesFilterDto.getRegionDto().getId(),
                        graduatesFilterDto.getHasPreviousEducationPlace(),
                        graduatesFilterDto.getMissedCertificateSendingCount()
                )
        );

    }

    @RequestMapping(value = "/status", method = RequestMethod.POST)
    public ResponseEntity<?> setGraduateReturnedMoney(@RequestBody GraduateAndReturnedMoneyStatusDto graduateAndReturnedMoneyStatusDto) {

        graduateService.setGraduateReturnedMoneyStatus(graduateAndReturnedMoneyStatusDto);

        return ResponseEntity.ok("Graduates status was changed successfully");
    }

}

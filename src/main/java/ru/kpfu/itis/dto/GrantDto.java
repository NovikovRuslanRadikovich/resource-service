package ru.kpfu.itis.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kpfu.itis.util.GrantValue;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GrantDto {
    Integer id;
    Integer courseNumber;
    Integer year;
    Double grantAmount;
    Integer course;
    GrantValue grantValue;
}

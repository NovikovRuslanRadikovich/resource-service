package ru.kpfu.itis.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GraduateByConditionDto {
    private GraduateDto graduateDto;
    private RegionDto regionDto;
    private GrantDto grantDto;
    private WorkingPlaceDto workingPlaceDto;
    private Integer numberOfWorkingPlaces;
}

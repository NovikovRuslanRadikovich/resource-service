package ru.kpfu.itis.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ArchiveFilterDto {

    Integer yearOfGraduation;
    RegionDto regionDto;
    Boolean hasPreviousEducationPlace;
}

package ru.kpfu.itis.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GraduateDto {
    Integer id;
    private String name;
    private String surname;
    private String patronymic;
    private String previousSurname;
    private Integer yearOfAdmission;
    private Integer yearOfGraduation;
    private String profileImageLocation;
}

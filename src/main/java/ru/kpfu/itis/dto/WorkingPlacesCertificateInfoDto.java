package ru.kpfu.itis.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kpfu.itis.util.CertificateName;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WorkingPlacesCertificateInfoDto {

    Integer id;
    Integer graduateId;
    Integer workingPlaceId;
    CertificateName certificateName;
}

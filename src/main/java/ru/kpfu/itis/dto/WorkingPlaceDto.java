package ru.kpfu.itis.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class WorkingPlaceDto {

    Integer id;
    String organizationName;
    String position;
    Date startDate;
    Date endDate;
    String orderOfAdmission;
    String inn;
    String kpp;
    Double mediumSalary;

}

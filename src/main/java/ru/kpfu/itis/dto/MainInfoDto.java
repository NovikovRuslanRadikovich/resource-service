package ru.kpfu.itis.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MainInfoDto {
    private String name;
    private String surname;
    private String patronymic;
    private String previousSurname;
    private String phoneNumber;
}

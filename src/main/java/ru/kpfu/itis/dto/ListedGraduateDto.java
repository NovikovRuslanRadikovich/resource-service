package ru.kpfu.itis.dto;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ListedGraduateDto {

    private GraduateDto graduateDto;
    private WorkingPlaceDto currentWorkingPlaceDto;
    private RegionDto regionDto;
    private List<GrantDto> grantDtoList;
    private Integer numberOfWorkingPlaces;
}

package ru.kpfu.itis.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AcademicLeaveDto {

    Integer id;
    Boolean isPresent;
    Integer leavingYear;
    Integer comingYear;

}

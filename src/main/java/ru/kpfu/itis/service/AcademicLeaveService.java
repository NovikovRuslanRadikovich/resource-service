package ru.kpfu.itis.service;

import ru.kpfu.itis.dto.AcademicLeaveDto;

public interface AcademicLeaveService {

    AcademicLeaveDto findByLeavingYearAndComingYear(Integer leavingYear, Integer comingYear);

    AcademicLeaveDto createAcademicLeave(AcademicLeaveDto academicLeaveDto);
}

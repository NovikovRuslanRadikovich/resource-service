package ru.kpfu.itis.service;

import ru.kpfu.itis.dto.ListedGraduateDto;

import java.io.IOException;
import java.util.List;

public interface ExcelService {

    byte[] graduatesToExcel(List<ListedGraduateDto> graduates) throws IOException;
}

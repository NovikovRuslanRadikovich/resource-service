package ru.kpfu.itis.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.entity.PreviousEducationPlace;
import ru.kpfu.itis.repository.PreviousEducationPlaceRepository;
import ru.kpfu.itis.service.PreviousEducationPlaceService;

@Service
public class PreviousEducationPlaceServiceImpl implements PreviousEducationPlaceService {

    private PreviousEducationPlaceRepository previousEducationPlaceEntityRepository;

    @Autowired
    PreviousEducationPlaceServiceImpl(PreviousEducationPlaceRepository previuosEducationPlaceEntityRepository) {
        this.previousEducationPlaceEntityRepository = previuosEducationPlaceEntityRepository;
    }


    @Override
    public PreviousEducationPlace createPreviousEducationPlace(PreviousEducationPlace previousEducationPlace) {
        return previousEducationPlaceEntityRepository.save(previousEducationPlace);
    }

    @Override
    public PreviousEducationPlace findByHighSchoolAndFaculty(String highSchool, String facultyName) {
        return previousEducationPlaceEntityRepository.findByHighSchoolAndFaculty(highSchool,facultyName);
    }
}

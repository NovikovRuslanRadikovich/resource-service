package ru.kpfu.itis.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.converter.GraduateConverter;
import ru.kpfu.itis.converter.GrantConverter;
import ru.kpfu.itis.converter.WorkingPlaceConverter;
import ru.kpfu.itis.dto.*;
import ru.kpfu.itis.entity.Graduate;
import ru.kpfu.itis.entity.Region;
import ru.kpfu.itis.repository.GraduateRepository;
import ru.kpfu.itis.service.GraduateService;
import ru.kpfu.itis.service.RegionService;
import ru.kpfu.itis.util.Constants;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class GraduateServiceImpl implements GraduateService {

    private GraduateConverter graduateConverter;
    private GraduateRepository graduateRepository;
    private RegionService regionService;
    private GrantConverter grantConverter;
    private WorkingPlaceConverter workingPlaceConverter;

    @Autowired
    GraduateServiceImpl(GraduateConverter graduateConverter, GraduateRepository graduateRepository, RegionService regionService, GrantConverter grantConverter, WorkingPlaceConverter workingPlaceConverter) {
        this.graduateConverter = graduateConverter;
        this.graduateRepository = graduateRepository;
        this.regionService = regionService;
        this.grantConverter = grantConverter;
        this.workingPlaceConverter = workingPlaceConverter;
    }

    @Override
    public GraduateDto createGraduate(Graduate graduate) {
        return graduateConverter.doForward(graduateRepository.save(graduate));
    }

    @Override
    public List<Graduate> getAllGraduates() {
        return graduateRepository.findAll();
    }

    @Override
    public void saveGraduate(Graduate graduate) {
        graduateRepository.save(graduate);
    }

    public List<ListedGraduateDto> getGraduatesInGrantReceiversPageWithoutMissedCertificateFilter(Integer yearOfGraduation, Integer workedYears, Integer regionId, Boolean hasPreviousEducationPlace) {

        List<Graduate> graduatesOfYear = graduateRepository.findAllByYearOfGraduation(yearOfGraduation);
        List<Graduate> graduatesByStatus = graduateRepository.findAllByStatus(workedYears);
        List<Graduate> graduatesByRegionId = graduateRepository.findAllByRegionId(regionId);
        List<Graduate> graduatesByHasPreviousEducationPlace = graduateRepository.findAllByHasPreviousEducationPlace(hasPreviousEducationPlace);

        List<Graduate> resultedGraduates = graduatesOfYear
                .stream()
                .filter(graduatesByStatus::contains)
                .filter(graduatesByRegionId::contains)
                .filter(graduatesByHasPreviousEducationPlace::contains)
                .collect(Collectors.toList());

        List<ListedGraduateDto> listedGraduateDtoList = new ArrayList<>();
        for(Graduate graduate : resultedGraduates) {
            ListedGraduateDto listedGraduateDto = new ListedGraduateDto();
            listedGraduateDto.setRegionDto(regionService.getRegionByCode(regionId));
            listedGraduateDto.setGrantDtoList(graduate.getGraduatesGrants().stream().map(grantConverter::doForward).collect(Collectors.toList()));
            listedGraduateDto.setCurrentWorkingPlaceDto(workingPlaceConverter.doForward(
                    graduate
                            .getWorkingPlaceList()
                            .stream()
                            .filter(w -> {return w.getIsLastWorkingPlace().equals(true);})
                            .collect(Collectors.toList())
                            .get(0))
            );
            listedGraduateDto.setNumberOfWorkingPlaces(graduate.getWorkingPlaceList().size());

            listedGraduateDtoList.add(listedGraduateDto);
        }

        return listedGraduateDtoList;
    }

    @Override
    public List<RegionAndStudentsNumberDto> getRegionsAndNumbersOfStudents() {
        List<Object[]> regionAndStudentsCountList = graduateRepository.getRegionsAndStudentsNumber();

        List<RegionAndStudentsNumberDto> regionAndStudentsNumberDtoList = new ArrayList<>();
        for (Object[] regionAndStudentsCount : regionAndStudentsCountList) {
            RegionAndStudentsNumberDto regionAndStudentsNumberDto = RegionAndStudentsNumberDto.builder().regionName((String) regionAndStudentsCount[0]).numberOfStudents((Integer) regionAndStudentsCount[1]).build();
            regionAndStudentsNumberDtoList.add(regionAndStudentsNumberDto);
        }

        return regionAndStudentsNumberDtoList;
    }

    @Override
    public Map<Integer, Long> getGraduatesCountByRegions() {
        List<Graduate> allGraduates = getAllGraduates();
        Map<Region, Long> graduatesCountByRegion = allGraduates.stream().collect(Collectors.groupingBy(Graduate::getRegion, Collectors.counting()));

        Map<Integer, Long> graduatesCountByRegionId = new HashMap<>();
        for(Map.Entry<Region,Long> entry : graduatesCountByRegion.entrySet()) {
            graduatesCountByRegionId.put(entry.getKey().getId(),entry.getValue());
        }
        return graduatesCountByRegionId;
    }

    @Override
    public GraduateDto findById(Integer id) {
        return graduateConverter.doForward(graduateRepository.findOne(id));
    }

    @Override
    public GraduateDto findByCurrentUserId(Integer currentUserId) {
        return graduateConverter.doForward(graduateRepository.findByUserId(currentUserId));
    }

    @Override
    public void updateProfileImageLocation(Integer graduateId, String fileLocation) {
        graduateRepository.updateProfileImage(graduateId, fileLocation);
    }



    @Override
    public List<ListedGraduateDto> getGraduatesWithMissedCertificateFilter(Integer yearOfGraduation, Integer status, Integer regionId, Boolean hasPreviousEducationPlace, Integer missedCertificateSendingCount) {
        List<ListedGraduateDto> listedGraduateDtoList = getGraduatesInGrantReceiversPageWithoutMissedCertificateFilter(yearOfGraduation,status,regionId,hasPreviousEducationPlace);
        for(ListedGraduateDto listedGraduateDto : listedGraduateDtoList) {
            if(!hasGraduateMissedCertificatesSendings(missedCertificateSendingCount, listedGraduateDto.getGraduateDto().getId()) || hasGraduateMissedCertificatesSendings(missedCertificateSendingCount + 1, listedGraduateDto.getGraduateDto().getId())   ) {
                listedGraduateDtoList.remove(listedGraduateDto);
            }
        }

        return listedGraduateDtoList;
    }

    @Override
    public List<ListedGraduateDto> getArchivedList(ArchiveFilterDto archiveFilterDto) {
        return getGraduatesWithMissedCertificateFilter(archiveFilterDto.getYearOfGraduation(), Constants.WORKED_YEARS,archiveFilterDto.getRegionDto().getId(),archiveFilterDto.getHasPreviousEducationPlace(),Constants.MISSED_CERTIFICATE_SENDING_COUNT_OF_WORKED_GRADUATE);
    }

    @Override
    public boolean hasGraduateMissedCertificatesSendings(int numberOfMissedSendings, int graduateId) {

        GraduateDto graduateDto = findById(graduateId);
        Integer yearOfAdmission = graduateDto.getYearOfAdmission();
        Date dateOfAdmission = new Date(yearOfAdmission, Constants.MONTH_OF_GRADUATION,Constants.DAY_OF_GRADUATION);
        Date currentDate = new Date();
        int numberOfPastDays = (int) (currentDate.getTime() - dateOfAdmission.getTime()) / (24 * 60 * 60 * 1000);
        int numberOfCertificatesOfEachTypeMustBeSentByGraduate = numberOfPastDays / 182;

        return numberOfCertificatesOfEachTypeMustBeSentByGraduate > getMinimumNumberOfCertificatesOfConcreteTypeSentByGraduate(graduateId);
    }

    @Override
    public List<GraduateDto> getGraduatesByYearOfGraduation(Integer yearOfGraduation) {

        List<Graduate> graduateList = graduateRepository.findAllByYearOfGraduation(yearOfGraduation);


        List<GraduateDto> graduateDtoList = new ArrayList<>();
        for(Graduate graduate : graduateList) {
            GraduateDto listedGraduateDto = new GraduateDto();

            listedGraduateDto.setId(graduate.getId());
            listedGraduateDto.setName(graduate.getName());
            listedGraduateDto.setSurname(graduate.getSurname());
            listedGraduateDto.setPreviousSurname(graduate.getPreviousSurname());
            listedGraduateDto.setPatronymic(graduate.getPatronymic());
            listedGraduateDto.setYearOfAdmission(graduate.getYearOfAdmission());
            listedGraduateDto.setYearOfGraduation(graduate.getYearOfGraduation());

            graduateDtoList.add(listedGraduateDto);
        }

        return graduateDtoList;
    }

    @Override
    public void setGraduateReturnedMoneyStatus(GraduateAndReturnedMoneyStatusDto graduateAndReturnedMoneyStatusDto) {
        graduateRepository.changeReturnedMoneyStatus(graduateAndReturnedMoneyStatusDto.getGraduateId(),graduateAndReturnedMoneyStatusDto.getHasReturnedMoney());
    }

    public Integer getMinimumNumberOfCertificatesOfConcreteTypeSentByGraduate(Integer graduateId) {
        int[] certificatesCountArray = new int[]{getNumberOfCertificatesOfExactTypeSentByGraduate(graduateId, "certificates/certificates_of_deductions"),
                getNumberOfCertificatesOfExactTypeSentByGraduate(graduateId, "certificates/certificates_of_employment"),
                getNumberOfCertificatesOfExactTypeSentByGraduate(graduateId, "certificates/certificates_of_insurrance_deductions_from_tax_service")};
        Arrays.sort(certificatesCountArray);

        return certificatesCountArray[0];
    }

    private int getNumberOfCertificatesOfExactTypeSentByGraduate(Integer graduateId, String certificatesDirectory) {

        int number = 0;
        ClassPathResource res = new ClassPathResource(certificatesDirectory);
        File directoryFile = new File(res.getPath());

        for (File file : Objects.requireNonNull(directoryFile.listFiles())) {
            String fileName = file.getName();

            String[] fileNameComponents = fileName.split("_");

            Integer currentGraduateId = Integer.valueOf(fileNameComponents[0]);
            if (graduateId.equals(currentGraduateId)) {
                number++;
            }
        }
        return number;
    }

}

package ru.kpfu.itis.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.dto.GraduateDto;
import ru.kpfu.itis.dto.TaxAmountAndYearDto;
import ru.kpfu.itis.service.GraduateService;
import ru.kpfu.itis.service.TaxService;
import ru.kpfu.itis.service.WorkingPlaceService;

import java.util.List;

@Service
public class TaxServiceImpl implements TaxService {

    WorkingPlaceService workingPlaceService;
    GraduateService graduateService;

    @Autowired
    TaxServiceImpl(WorkingPlaceService workingPlaceService, GraduateService graduateService) {
        this.workingPlaceService = workingPlaceService;
        this.graduateService = graduateService;
    }


    @Override
    public TaxAmountAndYearDto getTaxAmountsByYear(Integer yearOfGraduation) {
        Integer taxSum = 0;

        List<GraduateDto> graduatesByYearOfGraduation = graduateService.getGraduatesByYearOfGraduation(yearOfGraduation);

        for(GraduateDto graduateDto : graduatesByYearOfGraduation) {
            Integer graduateId = graduateDto.getId();

            taxSum += workingPlaceService.getEarnedMoneyByGraduate(graduateId);
        }

        Integer taxAmount = (taxSum / 100) * 13;

        return TaxAmountAndYearDto.builder().taxAmount(taxAmount).year(yearOfGraduation).build();
    }
}

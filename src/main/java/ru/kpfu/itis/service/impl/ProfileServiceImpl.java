package ru.kpfu.itis.service.impl;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.itis.config.ImagesLocationConfig;
import ru.kpfu.itis.converter.AcademicLeaveConverter;
import ru.kpfu.itis.converter.GraduateConverter;
import ru.kpfu.itis.converter.GrantConverter;
import ru.kpfu.itis.converter.RegionConverter;
import ru.kpfu.itis.dto.*;
import ru.kpfu.itis.entity.Graduate;
import ru.kpfu.itis.entity.Grant;
import ru.kpfu.itis.entity.PreviousEducationPlace;
import ru.kpfu.itis.exception.GraduateNotFoundException;
import ru.kpfu.itis.exception.PhotoUnloadingException;
import ru.kpfu.itis.exception.PhotoUploadingException;
import ru.kpfu.itis.service.*;
import ru.kpfu.itis.util.EmploymentError;

import javax.transaction.Transactional;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ProfileServiceImpl implements ProfileService {

    private RegionService regionService;
    private RegionConverter regionConverter;
    private GraduateService graduateService;
    private AcademicLeaveService academicLeaveService;
    private AcademicLeaveConverter academicLeaveConverter;
    private PreviousEducationPlaceService previousEducationPlaceService;
    private WorkingPlaceService workingPlaceService;
    private GrantService grantsService;
    private GraduateConverter graduateConverter;
    private GrantConverter grantConverter;
    private ImagesLocationConfig imagesLocationConfig;

    @Autowired
    ProfileServiceImpl(RegionService regionService, RegionConverter regionConverter, GraduateService graduateService, AcademicLeaveService academicLeaveService, AcademicLeaveConverter academicLeaveConverter, PreviousEducationPlaceService previousEducationPlaceService, WorkingPlaceService workingPlaceService, GrantService grantsService, GraduateConverter graduateConverter, GrantConverter grantConverter, ImagesLocationConfig imagesLocationConfig) {
        this.regionService = regionService;
        this.regionConverter = regionConverter;
        this.graduateService = graduateService;
        this.academicLeaveService = academicLeaveService;
        this.academicLeaveConverter = academicLeaveConverter;
        this.previousEducationPlaceService = previousEducationPlaceService;
        this.workingPlaceService = workingPlaceService;
        this.grantsService = grantsService;
        this.graduateConverter = graduateConverter;
        this.grantConverter = grantConverter;
        this.imagesLocationConfig = imagesLocationConfig;
    }

    @Override
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public void fulfillProfile(ProfileDto profileDto, Integer userId) {
        MainInfoDto mainInfoDto = profileDto.getGraduateInfoDto().getMainInfoDto();
        RegionDto regionDto = profileDto.getGraduateInfoDto().getRegionDto();
        List<GrantDto> grantDtoList = profileDto.getGraduateInfoDto().getGrantDtoList();
        EducationPeriodDto educationPeriodDto = profileDto.getGraduateInfoDto().getEducationPeriodDto();
        AcademicLeaveDto academicLeaveDto = profileDto.getGraduateInfoDto().getAcademicLeaveDto();
        PreviousEducationPlaceDto previousEducationPlaceDto = profileDto.getGraduateInfoDto().getPreviousEducationPlaceDto();

        RegionDto storedRegionDto = regionService.getRegionByCode(regionDto.getId());
        AcademicLeaveDto academicLeaveDtoCreatedOrRetreived;
        if (academicLeaveService.findByLeavingYearAndComingYear(academicLeaveDto.getLeavingYear(), academicLeaveDto.getComingYear()) == null) {
            academicLeaveDtoCreatedOrRetreived = academicLeaveService.createAcademicLeave(academicLeaveDto);
        } else {
            academicLeaveDtoCreatedOrRetreived = academicLeaveService.findByLeavingYearAndComingYear(academicLeaveDto.getLeavingYear(), academicLeaveDto.getComingYear());
        }
        PreviousEducationPlace previousEducationPlace = previousEducationPlaceService.createPreviousEducationPlace(
                PreviousEducationPlace
                        .builder()
                        .highSchool(previousEducationPlaceDto.getHighSchool())
                        .faculty(previousEducationPlaceDto.getFacultyName())
                        .build()
        );

        Set<GrantDto> grantDtoStoredSet = grantDtoList.stream().map(grantDto -> grantsService.createGrantIfNotExist(
                grantDto
                )
        ).collect(Collectors.toSet());

        Set<Grant> storedGrantSetForGraduate = grantDtoStoredSet.stream().map(grantDtoStored -> grantConverter.doBackward(grantDtoStored)).collect(Collectors.toSet());

        Graduate graduate = Graduate
                .builder()
                .name(mainInfoDto.getName())
                .surname(mainInfoDto.getSurname())
                .patronymic(mainInfoDto.getPatronymic())
                .previousSurname(mainInfoDto.getPreviousSurname())
                .phoneNumber(mainInfoDto.getPhoneNumber())
                .yearOfAdmission(educationPeriodDto.getYearOfAdmission())
                .yearOfGraduation(educationPeriodDto.getYearOfGraduation())
                .academicLeave(academicLeaveConverter.doBackward(academicLeaveDtoCreatedOrRetreived))
                .previousEducationPlace(previousEducationPlace)
                .region(regionConverter.doBackward(storedRegionDto))
                .graduatesGrants(storedGrantSetForGraduate)
                .userId(userId)
                .build();

        graduateService.createGraduate(graduate);
        workingPlaceService.createWorkingPlace(userId, profileDto.getWorkingPlaceDto());

    }

    @Override
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public void savePhoto(Integer currentUserId, MultipartFile photoFile) {

        Integer graduateId = graduateService.findByCurrentUserId(currentUserId).getId();
        String fileLocation = imagesLocationConfig.getProfileImagesFilesLocation() + "/" + graduateId + "_profileImage.";
        fileLocation = fileLocation + FilenameUtils.getExtension(photoFile.getOriginalFilename());
        fileLocation = System.getProperty("user.dir") + fileLocation;
        try {
            Files.deleteIfExists(Paths.get(fileLocation));
            File newFile = new File(fileLocation);
            photoFile.transferTo(newFile);
        } catch (IOException e) {
            throw new PhotoUploadingException(EmploymentError.IMAGE_NOT_UPLOADED);
        }

        graduateService.updateProfileImageLocation(graduateId, fileLocation);
    }

    @Override
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public void updateGraduateInfo(Integer currentUserId, GraduateInfoDto graduateInfoDto) {
        Graduate graduate = null;
        try {
            graduate = graduateConverter.doBackward(graduateService.findByCurrentUserId(currentUserId));
        } catch (NullPointerException ex) {
            throw new GraduateNotFoundException(EmploymentError.GRADUATE_NOT_CREATED);
        }
        graduate.setPhoneNumber(graduateInfoDto.getMainInfoDto().getPhoneNumber());
        graduate.setName(graduateInfoDto.getMainInfoDto().getName());
        graduate.setSurname(graduateInfoDto.getMainInfoDto().getSurname());
        graduate.setPatronymic(graduateInfoDto.getMainInfoDto().getPatronymic());

        AcademicLeaveDto academicLeaveDto = graduateInfoDto.getAcademicLeaveDto();
        if (academicLeaveDto.getIsPresent()) {
            AcademicLeaveDto academicLeaveDtoCreatedOrRetreived = academicLeaveService.findByLeavingYearAndComingYear(academicLeaveDto.getLeavingYear(), academicLeaveDto.getComingYear());
            if (academicLeaveDtoCreatedOrRetreived == null) {
                academicLeaveDtoCreatedOrRetreived = academicLeaveService.createAcademicLeave(academicLeaveDto);
            }
            graduate.setAcademicLeave(academicLeaveConverter.doBackward(academicLeaveDtoCreatedOrRetreived));
        } else {
            graduate.setAcademicLeave(null);
        }
        graduate.setRegion(regionConverter.doBackward(regionService.getRegionByCode(graduateInfoDto.getRegionDto().getId())));
        graduate.setYearOfAdmission(graduateInfoDto.getEducationPeriodDto().getYearOfAdmission());
        graduate.setYearOfGraduation(graduateInfoDto.getEducationPeriodDto().getYearOfGraduation());

        Set<GrantDto> grantDtoStoredSet = graduateInfoDto.getGrantDtoList().stream().map(grantDto -> grantsService.createGrantIfNotExist(grantDto)).collect(Collectors.toSet());

        Set<Grant> storedGrantSetForGraduate = grantDtoStoredSet.stream().map(grantDtoStored -> grantConverter.doBackward(grantDtoStored)).collect(Collectors.toSet());

        graduate.setGraduatesGrants(storedGrantSetForGraduate);

        PreviousEducationPlaceDto previousEducationPlaceDto = graduateInfoDto.getPreviousEducationPlaceDto();
        if (previousEducationPlaceDto.getIsPresent()) {
            PreviousEducationPlace previousEducationPlace = previousEducationPlaceService.findByHighSchoolAndFaculty(previousEducationPlaceDto.getFacultyName(), previousEducationPlaceDto.getHighSchool());
            if (previousEducationPlace == null) {
                previousEducationPlace = previousEducationPlaceService.createPreviousEducationPlace(PreviousEducationPlace
                        .builder()
                        .faculty(previousEducationPlaceDto.getFacultyName())
                        .highSchool(previousEducationPlaceDto.getHighSchool())
                        .build()

                );
            }
            graduate.setPreviousEducationPlace(previousEducationPlace);
        } else {
            graduate.setPreviousEducationPlace(null);
        }
    }

    @Override
    public InputStreamResource getImage(Integer userId) {
        GraduateDto graduateDto = graduateService.findByCurrentUserId(userId);
        String fileLocation = graduateDto.getProfileImageLocation();
        File file = new File(fileLocation);
        try {
            return new InputStreamResource(new FileInputStream(file));
        } catch (FileNotFoundException e) {
            throw new PhotoUnloadingException(EmploymentError.IMAGE_NOT_UNLOADED);
        }
    }
}

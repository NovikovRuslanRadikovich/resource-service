package ru.kpfu.itis.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.converter.AcademicLeaveConverter;
import ru.kpfu.itis.dto.AcademicLeaveDto;
import ru.kpfu.itis.entity.AcademicLeave;
import ru.kpfu.itis.repository.AcademicLeaveRepository;
import ru.kpfu.itis.service.AcademicLeaveService;


@Service
public class AcademicLeaveServiceImpl implements AcademicLeaveService {

    AcademicLeaveConverter academicLeaveConverter;
    AcademicLeaveRepository academicLeaveRepository;

    @Autowired
    AcademicLeaveServiceImpl(AcademicLeaveConverter academicLeaveConverter,AcademicLeaveRepository academicLeaveRepository) {
        this.academicLeaveConverter = academicLeaveConverter;
        this.academicLeaveRepository = academicLeaveRepository;
    }

    @Override
    public AcademicLeaveDto findByLeavingYearAndComingYear(Integer leavingYear, Integer comingYear) {
        return academicLeaveConverter.doForward(academicLeaveRepository.findByLeavingYearAndComingYear(leavingYear, comingYear));
    }

    @Override
    public AcademicLeaveDto createAcademicLeave(AcademicLeaveDto academicLeaveDto) {
        AcademicLeave academicLeave = academicLeaveConverter.doBackward(academicLeaveDto);
        return academicLeaveConverter.doForward(academicLeaveRepository.save(academicLeave));
    }
}

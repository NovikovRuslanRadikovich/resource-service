package ru.kpfu.itis.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.converter.GrantConverter;
import ru.kpfu.itis.dto.GrantDto;
import ru.kpfu.itis.entity.Grant;
import ru.kpfu.itis.repository.GrantRepository;
import ru.kpfu.itis.service.GrantService;

import javax.transaction.Transactional;

@Service
public class GrantServiceImpl implements GrantService {

    private GrantRepository grantRepository;
    private GrantConverter grantConverter;


    @Autowired
    GrantServiceImpl(GrantRepository grantRepository, GrantConverter grantConverter) {
        this.grantRepository = grantRepository;
        this.grantConverter = grantConverter;
    }

    @Override
    @Transactional(Transactional.TxType.REQUIRED)
    public GrantDto createGrantIfNotExist(GrantDto grantDto) {
        Grant retreivedGrant = grantRepository.getGrantsByYearOfReceivingAndGrantValue(grantDto.getYear(), grantDto.getGrantValue());

        return retreivedGrant != null ? grantConverter.doForward(retreivedGrant) : grantConverter.doForward(grantRepository.save(grantConverter.doBackward(grantDto)));
    }

}

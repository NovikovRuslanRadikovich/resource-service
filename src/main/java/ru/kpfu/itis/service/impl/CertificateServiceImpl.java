package ru.kpfu.itis.service.impl;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.itis.config.CertificatesLocationConfig;
import ru.kpfu.itis.converter.CertificateInfoConverter;
import ru.kpfu.itis.converter.GraduateConverter;
import ru.kpfu.itis.converter.WorkingPlaceConverter;
import ru.kpfu.itis.dto.WorkingPlacesCertificateInfoDto;
import ru.kpfu.itis.entity.CertificateInfo;
import ru.kpfu.itis.entity.Graduate;
import ru.kpfu.itis.entity.WorkingPlace;
import ru.kpfu.itis.exception.GraduateNotFoundException;
import ru.kpfu.itis.exception.WrongCertificateFormatException;
import ru.kpfu.itis.repository.CertificateInfoRepository;
import ru.kpfu.itis.service.CertificateService;
import ru.kpfu.itis.service.GraduateService;
import ru.kpfu.itis.service.WorkingPlaceService;
import ru.kpfu.itis.util.CertificateName;
import ru.kpfu.itis.util.EmploymentError;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CertificateServiceImpl implements CertificateService {

    CertificatesLocationConfig certificatesLocationConfig;
    CertificateInfoRepository certificateInfoRepository;
    WorkingPlaceService workingPlaceService;
    WorkingPlaceConverter workingPlaceConverter;
    GraduateService graduateService;
    GraduateConverter graduateConverter;
    CertificateInfoConverter certificateInfoConverter;

    @Autowired
    CertificateServiceImpl(CertificatesLocationConfig certificatesLocationConfig, CertificateInfoRepository certificateInfoRepository, WorkingPlaceService workingPlaceService, WorkingPlaceConverter workingPlaceConverter, GraduateService graduateService, GraduateConverter graduateConverter, CertificateInfoConverter certificateInfoConverter) {
        this.certificatesLocationConfig = certificatesLocationConfig;
        this.certificateInfoRepository = certificateInfoRepository;
        this.workingPlaceService = workingPlaceService;
        this.workingPlaceConverter = workingPlaceConverter;
        this.graduateService = graduateService;
        this.graduateConverter = graduateConverter;
        this.certificateInfoConverter = certificateInfoConverter;
    }

    @Override
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public void saveCertificates(Integer userId, MultipartFile certificateOfEmploymentFile, MultipartFile certificateOfDeductions, MultipartFile certificateOfInsuranceDeductionsFromTaxService) throws IOException {
        Integer graduateId;
        try {
            graduateId = graduateService.findByCurrentUserId(userId).getId();
        } catch (NullPointerException ex) {
             throw new GraduateNotFoundException(EmploymentError.GRADUATE_NOT_CREATED);
        }
        Integer workingPlaceId = workingPlaceService.getCurrentWorkingPlace(graduateId).getId();

        saveCertificate(graduateId, workingPlaceId, CertificateName.certificateOfEmploymentFile, certificateOfEmploymentFile);
        saveCertificate(graduateId, workingPlaceId, CertificateName.certificateOfDeductions, certificateOfDeductions);
        saveCertificate(graduateId, workingPlaceId, CertificateName.certificateOfInsuranceDeductionsFromTaxService, certificateOfInsuranceDeductionsFromTaxService);
    }

    @Override
    public WorkingPlacesCertificateInfoDto getCertificateInfoByGraduateIdAndWorkingPlaceIdAndCertificateName(Integer graduateId, Integer workingPlaceId, CertificateName certificateName) {

        return certificateInfoConverter.doForward(certificateInfoRepository.getCertificateInfoByGraduateAndWorkingPlaceAndCertificateName(graduateId, workingPlaceId, certificateName));
        //ToDo
    }

    @Override
    @Transactional(Transactional.TxType.REQUIRED)
    public void saveCertificateInfo(Integer graduateId, Integer workingPlaceId, String locationOfCertificate) {

        CertificateInfo certificateInfo = CertificateInfo
                .builder()
                .graduate(graduateConverter.doBackward(graduateService.findById(graduateId)))
                .locationOfCertificate(locationOfCertificate)
                .workingPlace(workingPlaceConverter.doBackward(workingPlaceService.findById(workingPlaceId)))
                .build();

        certificateInfoRepository.save(certificateInfo);

    }

    @Override
    public List<WorkingPlacesCertificateInfoDto> getCertificateInfoByGraduateIdAndWorkingPlaceId(Integer graduateId, Integer workingPlaceId) {
        Graduate graduate = graduateConverter.doBackward(graduateService.findById(graduateId));
        WorkingPlace workingPlace = workingPlaceConverter.doBackward(workingPlaceService.findById(workingPlaceId));

        List<WorkingPlacesCertificateInfoDto> certificateInfoDtoList = certificateInfoRepository.getCertificateInfoByGraduateAndWorkingPlace(graduate, workingPlace)
                .stream()
                .map(certificateInfoConverter::doForward)
                .collect(Collectors.toList());

        return certificateInfoDtoList;
    }

    @Transactional(Transactional.TxType.REQUIRED)
    public void saveCertificate(Integer graduateId, Integer workingPlaceId, CertificateName certificateName, MultipartFile file) throws IOException {
        if (file == null) {
            return;
        }
        java.util.Date date = new java.util.Date();
        int monthNumber = date.getMonth();
        int half;
        if (monthNumber < 7) {
            half = 1;
        } else {
            half = 2;
        }
        String fileLocation = null;
        switch (certificateName) {
            case certificateOfEmploymentFile:
                fileLocation = certificatesLocationConfig.getCertificateOfEmploymentFilesLocation() + "/" + graduateId + "_" + date.getYear() + "_" + half + ".";
                break;
            case certificateOfDeductions:
                fileLocation = certificatesLocationConfig.getCertificateOfDeductionsFilesLocation() + "/" + graduateId + "_" + date.getYear() + "_" + half + ".";
                break;
            case certificateOfInsuranceDeductionsFromTaxService:
                fileLocation = certificatesLocationConfig.getCertificateOfInsuranceDeductionsFromTaxServiceFilesLocation() + "/" + graduateId + "_" + date.getYear() + "_" + half + ".";
                break;
        }
        fileLocation = fileLocation + FilenameUtils.getExtension(file.getOriginalFilename());
        fileLocation = System.getProperty("user.dir") + fileLocation;
        File newFile = new File(fileLocation);
        file.transferTo(newFile);

        saveCertificateInfo(graduateId, workingPlaceId, fileLocation);
    }


    public void checkCertificateFormat(MultipartFile file) {
        if(!file.getContentType().equals("application/pdf")) {
            throw new WrongCertificateFormatException(EmploymentError.CERTIFICATE_FORMAT_IS_NOT_PDF_OR_DOCX);
        }
    }

}

package ru.kpfu.itis.service.impl;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.dto.ListedGraduateDto;
import ru.kpfu.itis.service.ExcelService;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

@Service
public class ExcelServiceImpl implements ExcelService {

    public byte[] graduatesToExcel(List<ListedGraduateDto> graduates) throws IOException {
        String[] Columns = {"Name", "Surname", "Patronymic", "First Course","Second Course","Third Course","Fourth Course","Organization Name", "Position","Start Date","Order Of Admission", "Number of working places"};
        try(Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream()){
            CreationHelper createHelper = workbook.getCreationHelper();
            Sheet sheet = workbook.createSheet("Graduates");
            Font headerFont = workbook.createFont();
            headerFont.setBold(true);
            headerFont.setColor(IndexedColors.BLUE.getIndex());
            CellStyle headerCellStyle = workbook.createCellStyle();
            headerCellStyle.setFont(headerFont);
            // Row for Header
            Row headerRow = sheet.createRow(0);
            // Header
            for (int col = 0; col < Columns.length; col++) {
                Cell cell = headerRow.createCell(col);
                cell.setCellValue(Columns[col]);
                cell.setCellStyle(headerCellStyle);
            }
            // CellStyle for Date
            CellStyle dateStyle = workbook.createCellStyle();
            dateStyle.setDataFormat(createHelper.createDataFormat().getFormat("#"));
            int rowIdx = 1;
            for (ListedGraduateDto listedGraduateDto : graduates) {
                Row row = sheet.createRow(rowIdx++);
                row.createCell(0).setCellValue(listedGraduateDto.getGraduateDto().getName());
                row.createCell(1).setCellValue(listedGraduateDto.getGraduateDto().getSurname());
                row.createCell(2).setCellValue(listedGraduateDto.getGraduateDto().getPatronymic());

                for(int i = 0; i < 3 ; i++) {
                    int finalI = i;
                    row.createCell(3 + i).setCellValue(listedGraduateDto.getGrantDtoList().stream().filter(grantDto -> grantDto.getCourse() == (finalI +1)).findFirst().toString());
                }

                row.createCell(7).setCellValue(listedGraduateDto.getCurrentWorkingPlaceDto().getOrganizationName());
                row.createCell(8).setCellValue(listedGraduateDto.getCurrentWorkingPlaceDto().getPosition());

                Cell startDateCell = row.createCell(9);
                startDateCell.setCellValue(listedGraduateDto.getCurrentWorkingPlaceDto().getStartDate());
                startDateCell.setCellStyle(dateStyle);

                Cell endDateCell = row.createCell(10);
                endDateCell.setCellValue(listedGraduateDto.getCurrentWorkingPlaceDto().getStartDate());
                endDateCell.setCellStyle(dateStyle);

                row.createCell(11).setCellValue(listedGraduateDto.getCurrentWorkingPlaceDto().getOrderOfAdmission());
                row.createCell(12).setCellValue(listedGraduateDto.getNumberOfWorkingPlaces());

            }
            workbook.write(out);
            return out.toByteArray();
        }
    }
}

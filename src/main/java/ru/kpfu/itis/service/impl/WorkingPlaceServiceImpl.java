package ru.kpfu.itis.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.converter.WorkingPlaceConverter;
import ru.kpfu.itis.dto.EmployerAndStudentsNumberDto;
import ru.kpfu.itis.dto.GraduateDto;
import ru.kpfu.itis.dto.WorkingPlaceDto;
import ru.kpfu.itis.dto.WorkingPlaceWithCertificatesInfoDto;
import ru.kpfu.itis.entity.WorkingPlace;
import ru.kpfu.itis.exception.GraduateNotFoundException;
import ru.kpfu.itis.exception.LastDayOfPreviousWorkingPlaceNotSetException;
import ru.kpfu.itis.repository.WorkingPlaceRepository;
import ru.kpfu.itis.service.GraduateService;
import ru.kpfu.itis.service.WorkingPlaceService;
import ru.kpfu.itis.util.EmploymentError;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class WorkingPlaceServiceImpl implements WorkingPlaceService {

    private WorkingPlaceConverter workingPlaceConverter;
    private WorkingPlaceRepository workingPlaceRepository;
    private GraduateService graduateService;

    @Autowired
    WorkingPlaceServiceImpl(WorkingPlaceConverter workingPlaceConverter, WorkingPlaceRepository workingPlaceRepository, GraduateService graduateService) {
        this.workingPlaceConverter = workingPlaceConverter;
        this.workingPlaceRepository = workingPlaceRepository;
        this.graduateService = graduateService;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public WorkingPlaceDto createWorkingPlace(Integer currentUserId, WorkingPlaceDto workingPlaceDto) {
        Integer graduateId = getGraduateId(currentUserId);

        Date endDate = workingPlaceRepository.getEndDateOfLastWorkingPlace(graduateId);
        if (endDate == null) {
            throw new LastDayOfPreviousWorkingPlaceNotSetException(EmploymentError.END_DATE_OF_PREVIOUS_WORKING_PLACE_NOT_SPECIFIED);
        }
        workingPlaceRepository.setNotLastWorkingPlaceForGraduate(graduateId);
        WorkingPlace workingPlace = workingPlaceConverter.doBackward(workingPlaceDto);
        workingPlace.setIsLastWorkingPlace(true);
        WorkingPlace workingPlaceCreated = workingPlaceRepository.save(workingPlace);
        workingPlaceRepository.setGraduateIdForWorkingPlace(graduateId, workingPlaceDto.getId());
        return workingPlaceConverter.doForward(workingPlaceCreated);
    }

    @Override
    public List<EmployerAndStudentsNumberDto> getEmployersAndNumberOfStudents() {
        List<Object[]> employersAndStudentsCount = workingPlaceRepository.getNumbersOfEmployeesByOrganizations();
        List<EmployerAndStudentsNumberDto> employerDtoList = new ArrayList<>();
        for (Object[] o : employersAndStudentsCount) {
            EmployerAndStudentsNumberDto employerDto = EmployerAndStudentsNumberDto.builder().organizationName((String) o[0]).numberOfStudents((Integer) o[1]).build();
            employerDtoList.add(employerDto);
        }
        return employerDtoList;
    }

    @Override
    public List<WorkingPlaceDto> getHistoryOfJobApplicationsByCurrentUser(Integer userId) {
        Integer graduateId = graduateService.findByCurrentUserId(userId).getId();
        List<WorkingPlace> workingPlaceList = workingPlaceRepository.getWorkingPlaceListByGraduate(graduateId);
        return workingPlaceList.stream().map(workingPlace -> workingPlaceConverter.doForward(workingPlace)).collect(Collectors.toList());
    }

    @Override
    public Integer getNumberOfJobApplicationsByCurrentUser(Integer userId) {
        Integer graduateId = graduateService.findByCurrentUserId(userId).getId();
        List<WorkingPlace> workingPlaceList = workingPlaceRepository.getWorkingPlaceListByGraduate(graduateId);
        return workingPlaceList.stream().collect(Collectors.groupingBy(WorkingPlace::getOrderOfAdmission, Collectors.toSet())).keySet().size();
    }

    @Override
    public List<WorkingPlaceWithCertificatesInfoDto> getWorkingPlaceWithCertificatesInfo(Integer userId) {

        List<WorkingPlaceDto> workingPlaceDtoList = getHistoryOfJobApplicationsByCurrentUser(userId);

        final List collect = workingPlaceDtoList
                .stream()
                .map(
                        (workingPlaceDto) -> {
                            return
                                    WorkingPlaceWithCertificatesInfoDto
                                            .builder()
                                            .workingPlaceDto(workingPlaceDto)
                                            .build();
                        }

                )
                .collect(Collectors.toList());
        return (List<WorkingPlaceWithCertificatesInfoDto>) collect;
    }

    @Override
    public WorkingPlaceDto findById(Integer workingPlaceId) {
        return workingPlaceConverter.doForward(workingPlaceRepository.getById(workingPlaceId));
    }

    @Override
    public WorkingPlaceDto getCurrentWorkingPlace(Integer graduateId) {
        GraduateDto graduateDto = graduateService.findById(graduateId);
        return workingPlaceConverter.doForward(workingPlaceRepository.findByGraduateIdAndIsLastWorkingPlace(graduateDto.getId(), true));
    }

    @Override
    public Integer getEarnedMoneyByGraduate(Integer graduateId) {
        long earnedMoney = 0;

        List<WorkingPlace> workingPlaceList = workingPlaceRepository.getWorkingPlaceListByGraduateId(graduateId);

        for (WorkingPlace workingPlace : workingPlaceList) {
            if (workingPlace.getStartDate() != null && workingPlace.getEndDate() != null) {
                earnedMoney = earnedMoney + (workingPlace.getEndDate().getTime() - workingPlace.getStartDate().getTime()) / (1000 * 60 * 60 * 24);
            } else if (workingPlace.getEndDate() == null) {
                earnedMoney = earnedMoney + (new Date().getTime() - workingPlace.getStartDate().getTime()) / (1000 * 60 * 60 * 24);
            }
        }
        return Math.toIntExact(earnedMoney);
    }

    @Override
    public WorkingPlaceDto updateLastWorkingPlace(Integer currentUserId, WorkingPlaceDto workingPlaceDto) {

        Integer graduateId = getGraduateId(currentUserId);

        workingPlaceRepository.setNotLastWorkingPlaceForGraduate(graduateId);
        WorkingPlace workingPlace = workingPlaceConverter.doBackward(workingPlaceDto);
        workingPlace.setIsLastWorkingPlace(true);
        WorkingPlace workingPlaceCreated = workingPlaceRepository.save(workingPlace);

        workingPlaceRepository.setGraduateIdForWorkingPlace(graduateId, workingPlaceDto.getId());
        return workingPlaceConverter.doForward(workingPlaceCreated);

    }

    private Integer getGraduateId(Integer currentUserId) {
        Integer graduateId;
        try {
            graduateId = graduateService.findByCurrentUserId(currentUserId).getId();
        } catch (NullPointerException ex) {
            throw new GraduateNotFoundException(EmploymentError.GRADUATE_NOT_CREATED);
        }
        return graduateId;
    }

}

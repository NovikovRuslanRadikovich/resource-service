package ru.kpfu.itis.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.converter.RegionConverter;
import ru.kpfu.itis.dto.RegionDto;
import ru.kpfu.itis.entity.Region;
import ru.kpfu.itis.repository.RegionRepository;
import ru.kpfu.itis.service.RegionService;

@Service
public class RegionServiceImpl implements RegionService {

    private RegionRepository regionRepository;
    private RegionConverter regionConverter;


    @Autowired
    RegionServiceImpl(RegionRepository regionRepository, RegionConverter regionConverter) {
        this.regionRepository = regionRepository;
        this.regionConverter = regionConverter;
    }

    @Override
    public RegionDto getRegionByCode(Integer code) {
        Region region = regionRepository.getByRegionCode(code);


        //ToDo make only get without save
        regionRepository.save(Region.builder().regionCode(code).build());

        return RegionDto.builder().id(region.getId()).nativeRegion(region.getNativeRegion()).regionCode(region.getRegionCode()).build();
    }
}

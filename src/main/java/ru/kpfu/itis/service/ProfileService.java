package ru.kpfu.itis.service;

import org.springframework.core.io.InputStreamResource;
import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.itis.dto.GraduateInfoDto;
import ru.kpfu.itis.dto.ProfileDto;

import java.io.IOException;

public interface ProfileService {

    void fulfillProfile(ProfileDto portfolioDto, Integer userId);

    void savePhoto(Integer userId, MultipartFile photoFile);

    void updateGraduateInfo(Integer userId,GraduateInfoDto graduateInfoDto);

    InputStreamResource getImage(Integer userId);

}

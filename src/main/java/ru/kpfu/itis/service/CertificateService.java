package ru.kpfu.itis.service;


import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.itis.dto.WorkingPlacesCertificateInfoDto;
import ru.kpfu.itis.util.CertificateName;

import java.io.IOException;
import java.util.List;

public interface CertificateService {

    void saveCertificates(Integer userId, MultipartFile certificateOfEmploymentFile, MultipartFile certificateOfDeductions, MultipartFile certificateOfInsuranceDeductionsFromTaxService) throws IOException;

    WorkingPlacesCertificateInfoDto getCertificateInfoByGraduateIdAndWorkingPlaceIdAndCertificateName(Integer graduateId, Integer workingPlaceId, CertificateName certificateName);

    void saveCertificateInfo(Integer graduateId, Integer workingPlaceId, String locationOfCertificate);

    List<WorkingPlacesCertificateInfoDto> getCertificateInfoByGraduateIdAndWorkingPlaceId(Integer graduateId, Integer workingPlaceId);
}

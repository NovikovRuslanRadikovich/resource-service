package ru.kpfu.itis.service;

import ru.kpfu.itis.dto.*;
import ru.kpfu.itis.entity.Graduate;

import java.util.List;
import java.util.Map;

public interface GraduateService {
    GraduateDto createGraduate(Graduate graduate);

    List<Graduate> getAllGraduates();

    void saveGraduate(Graduate graduate);

    public List<ListedGraduateDto> getGraduatesInGrantReceiversPageWithoutMissedCertificateFilter(Integer yearOfGraduation, Integer status, Integer region_id, Boolean hasPreviousEducationPlace);

    List<RegionAndStudentsNumberDto> getRegionsAndNumbersOfStudents();

    Map<Integer,Long> getGraduatesCountByRegions();

    GraduateDto findById(Integer id);

    GraduateDto findByCurrentUserId(Integer currentUserId);

    void updateProfileImageLocation(Integer graduateId, String fileLocation);

    List<ListedGraduateDto> getGraduatesWithMissedCertificateFilter(Integer yearOfGraduation, Integer status, Integer regionId, Boolean hasPreviousEducationPlace, Integer missedCertificateSendingCount);

    List<ListedGraduateDto> getArchivedList(ArchiveFilterDto archiveFilterDto);

    boolean hasGraduateMissedCertificatesSendings(int numberOfMissedSendings, int graduateId);

    List<GraduateDto> getGraduatesByYearOfGraduation(Integer yearOfGraduation);


    void setGraduateReturnedMoneyStatus(GraduateAndReturnedMoneyStatusDto graduateAndReturnedMoneyStatusDto);
}

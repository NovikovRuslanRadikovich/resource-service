package ru.kpfu.itis.service;

import ru.kpfu.itis.dto.GrantDto;

public interface GrantService {
     GrantDto createGrantIfNotExist(GrantDto grantDto);
}

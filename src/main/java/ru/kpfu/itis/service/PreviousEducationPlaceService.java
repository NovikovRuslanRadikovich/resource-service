package ru.kpfu.itis.service;

import ru.kpfu.itis.entity.PreviousEducationPlace;

public interface PreviousEducationPlaceService {

    PreviousEducationPlace createPreviousEducationPlace(PreviousEducationPlace build);

    PreviousEducationPlace findByHighSchoolAndFaculty(String highSchool, String facultyName);
}

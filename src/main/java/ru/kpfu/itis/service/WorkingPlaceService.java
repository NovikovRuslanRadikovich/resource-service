package ru.kpfu.itis.service;

import ru.kpfu.itis.dto.EmployerAndStudentsNumberDto;
import ru.kpfu.itis.dto.WorkingPlaceDto;
import ru.kpfu.itis.dto.WorkingPlaceWithCertificatesInfoDto;

import java.util.List;

public interface WorkingPlaceService {

    WorkingPlaceDto createWorkingPlace(Integer currentUserId, WorkingPlaceDto workingPlaceDto);

    List<EmployerAndStudentsNumberDto> getEmployersAndNumberOfStudents();

    List<WorkingPlaceDto> getHistoryOfJobApplicationsByCurrentUser(Integer currentUserId);

    public Integer getNumberOfJobApplicationsByCurrentUser(Integer  userId);

    List<WorkingPlaceWithCertificatesInfoDto> getWorkingPlaceWithCertificatesInfo(Integer userId);

    WorkingPlaceDto findById(Integer id);

    WorkingPlaceDto getCurrentWorkingPlace(Integer graduateId);

    Integer getEarnedMoneyByGraduate(Integer graduateId);

    WorkingPlaceDto updateLastWorkingPlace(Integer currentUserId, WorkingPlaceDto workingPlaceDto);
}

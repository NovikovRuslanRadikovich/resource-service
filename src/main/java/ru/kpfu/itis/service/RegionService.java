package ru.kpfu.itis.service;

import ru.kpfu.itis.dto.RegionDto;

public interface RegionService {

    RegionDto getRegionByCode(Integer code);

}

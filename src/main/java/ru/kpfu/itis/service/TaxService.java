package ru.kpfu.itis.service;

import ru.kpfu.itis.dto.TaxAmountAndYearDto;

public interface TaxService {

    TaxAmountAndYearDto getTaxAmountsByYear(Integer yearOfGraduation);
}

package ru.kpfu.itis.exception;

public class PhotoUploadingException extends RuntimeException {

    public PhotoUploadingException(String message) {
        super(message);
    }
}

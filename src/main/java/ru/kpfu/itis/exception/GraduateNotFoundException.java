package ru.kpfu.itis.exception;


public class GraduateNotFoundException extends RuntimeException {

    public GraduateNotFoundException(String message) {
        super(message);
    }

}

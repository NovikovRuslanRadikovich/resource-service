package ru.kpfu.itis.exception;

public class PhotoUnloadingException extends RuntimeException {

    public PhotoUnloadingException(String message) {
        super(message);
    }
}
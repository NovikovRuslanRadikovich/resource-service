package ru.kpfu.itis.exception;

public class WrongCertificateFormatException extends RuntimeException {

    public WrongCertificateFormatException(String message) {
        super(message);
    }
}

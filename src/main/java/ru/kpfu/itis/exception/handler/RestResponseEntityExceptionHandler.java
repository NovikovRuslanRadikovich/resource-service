package ru.kpfu.itis.exception.handler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.kpfu.itis.exception.*;

@ControllerAdvice
public class RestResponseEntityExceptionHandler
        extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { GraduateNotFoundException.class, LastDayOfPreviousWorkingPlaceNotSetException.class,
            WrongCertificateFormatException.class, PhotoUploadingException.class,
            PhotoUnloadingException.class})
    protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
        String bodyOfResponse = ex.getMessage();

        if(ex instanceof GraduateNotFoundException) {
            return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.NOT_FOUND, request);
        } else if(ex instanceof LastDayOfPreviousWorkingPlaceNotSetException) {
            return handleExceptionInternal(ex, bodyOfResponse,new HttpHeaders(), HttpStatus.FORBIDDEN, request);
        } else if(ex instanceof PhotoUploadingException) {
            return handleExceptionInternal(ex, bodyOfResponse,new HttpHeaders(), HttpStatus.FORBIDDEN, request);
        } else if(ex instanceof PhotoUnloadingException) {
            return handleExceptionInternal(ex, bodyOfResponse,new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
        }

        return null;
    }

}

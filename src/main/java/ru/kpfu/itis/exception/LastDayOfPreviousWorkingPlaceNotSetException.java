package ru.kpfu.itis.exception;

public class LastDayOfPreviousWorkingPlaceNotSetException extends RuntimeException {

    public LastDayOfPreviousWorkingPlaceNotSetException(String message) {
        super(message);
    }
}

package ru.kpfu.itis.converter;

import org.springframework.stereotype.Component;
import ru.kpfu.itis.dto.WorkingPlaceDto;
import ru.kpfu.itis.entity.WorkingPlace;

import static java.util.Objects.isNull;

@Component
public class WorkingPlaceConverter {

    public WorkingPlaceDto doForward(WorkingPlace workingPlace) {
        if (isNull(workingPlace)) {
            return null;
        }
        return WorkingPlaceDto
                .builder()
                .id(workingPlace.getId())
                .mediumSalary(workingPlace.getMediumSalary())
                .orderOfAdmission(workingPlace.getOrderOfAdmission())
                .startDate(workingPlace.getStartDate())
                .endDate(workingPlace.getEndDate())
                .position(workingPlace.getPosition())
                .inn(workingPlace.getInn())
                .kpp(workingPlace.getKpp())
                .organizationName(workingPlace.getOrganizationName())
                .build();

    }

    public WorkingPlace doBackward(WorkingPlaceDto workingPlaceDto) {
        if (isNull(workingPlaceDto)) {
            return null;
        }

        return WorkingPlace
                .builder()
//                .id(workingPlaceDto.getId())
                .mediumSalary(workingPlaceDto.getMediumSalary())
                .orderOfAdmission(workingPlaceDto.getOrderOfAdmission())
                .startDate(workingPlaceDto.getStartDate())
                .endDate(workingPlaceDto.getEndDate())
                .position(workingPlaceDto.getPosition())
                .inn(workingPlaceDto.getInn())
                .kpp(workingPlaceDto.getKpp())
                .organizationName(workingPlaceDto.getOrganizationName())
                .build();

    }
}

package ru.kpfu.itis.converter;


import org.springframework.stereotype.Component;
import ru.kpfu.itis.dto.GraduateDto;
import ru.kpfu.itis.entity.Graduate;

import static java.util.Objects.isNull;

@Component
public class GraduateConverter {

    public GraduateDto doForward(Graduate graduate) {
        if (isNull(graduate)) {
            return null;
        }
        return GraduateDto
                .builder()
                .id(graduate.getId())
                .name(graduate.getName())
                .surname(graduate.getSurname())
                .patronymic(graduate.getPatronymic())
                .previousSurname(graduate.getPreviousSurname())
                .yearOfAdmission(graduate.getYearOfAdmission())
                .yearOfGraduation(graduate.getYearOfGraduation())
                .profileImageLocation(graduate.getProfileImageLocation())
                .build();

    }

    public Graduate doBackward(GraduateDto graduateDto) {
        if (isNull(graduateDto )) {
            return null;
        }

        return Graduate
                .builder()
                .name(graduateDto.getName())
                .surname(graduateDto.getSurname())
                .patronymic(graduateDto.getPatronymic())
                .previousSurname(graduateDto.getPreviousSurname())
                .yearOfGraduation(graduateDto.getYearOfGraduation())
                .yearOfAdmission(graduateDto.getYearOfAdmission())
                .build();

    }


}

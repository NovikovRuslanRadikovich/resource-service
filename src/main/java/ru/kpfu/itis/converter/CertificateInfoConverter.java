package ru.kpfu.itis.converter;

import org.springframework.stereotype.Component;
import ru.kpfu.itis.dto.WorkingPlacesCertificateInfoDto;
import ru.kpfu.itis.entity.CertificateInfo;

import static java.util.Objects.isNull;

@Component
public class CertificateInfoConverter {

    public WorkingPlacesCertificateInfoDto doForward(CertificateInfo certificateInfo) {
        if (isNull(certificateInfo)) {
            return null;
        }
        return WorkingPlacesCertificateInfoDto
                .builder()
                .id(certificateInfo.getId())
                .certificateName(certificateInfo.getCertificateName())
                .graduateId(certificateInfo.getGraduate().getId())
                .workingPlaceId(certificateInfo.getWorkingPlace().getId())
                .build();

    }



}

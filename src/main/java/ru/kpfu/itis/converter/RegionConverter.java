package ru.kpfu.itis.converter;


import org.springframework.stereotype.Component;
import ru.kpfu.itis.dto.RegionDto;
import ru.kpfu.itis.entity.Region;

import static java.util.Objects.isNull;

@Component
public class RegionConverter {

    public RegionDto doForward(Region region) {
        if (isNull(region)) {
            return null;
        }
        return RegionDto
                .builder()
                .id(region.getId())
                .regionCode(region.getRegionCode())
                .build();

    }

    public Region doBackward(RegionDto regionDto) {
        if (isNull(regionDto )) {
            return null;
        }

        return Region
                .builder()
                .nativeRegion(regionDto.getNativeRegion())
                .regionCode(regionDto.getRegionCode())
                .build();
    }

}

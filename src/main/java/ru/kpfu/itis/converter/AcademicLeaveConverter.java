package ru.kpfu.itis.converter;

import org.springframework.stereotype.Component;
import ru.kpfu.itis.dto.AcademicLeaveDto;
import ru.kpfu.itis.entity.AcademicLeave;

import static java.util.Objects.isNull;

@Component
public class AcademicLeaveConverter {


    public AcademicLeaveDto doForward(AcademicLeave academicLeave) {
        if (isNull(academicLeave)) {
            return null;
        }
        return AcademicLeaveDto
                .builder()
                .id(academicLeave.getId())
                .isPresent(true)
                .comingYear(academicLeave.getComingYear())
                .leavingYear(academicLeave.getLeavingYear())
                .build();

    }

    public AcademicLeave doBackward(AcademicLeaveDto academicLeaveDto) {
        if (isNull(academicLeaveDto )) {
            return null;
        }

        return AcademicLeave
                .builder()
                .comingYear(academicLeaveDto.getComingYear())
                .leavingYear(academicLeaveDto.getLeavingYear())
                .build();

    }
}

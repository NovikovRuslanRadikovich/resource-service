package ru.kpfu.itis.converter;


import org.springframework.stereotype.Component;
import ru.kpfu.itis.dto.GrantDto;
import ru.kpfu.itis.entity.Grant;

import static java.util.Objects.isNull;

@Component
public class GrantConverter {

    public GrantDto doForward(Grant grant) {
        if (isNull(grant)) {
            return null;
        }
        return GrantDto
                .builder()
                .id(grant.getId())
                .courseNumber(grant.getCourseNumber())
                .grantAmount(grant.getGrantAmount())
                .grantValue(grant.getGrantValue())
                .year(grant.getYearOfReceiving())
                .build();

    }

    public Grant doBackward(GrantDto grantDto) {
        if (isNull(grantDto )) {
            return null;
        }

        return Grant
                .builder()
                .grantValue(grantDto.getGrantValue())
                .grantAmount(grantDto.getGrantAmount())
                .yearOfReceiving(grantDto.getYear())
                .courseNumber(grantDto.getCourseNumber())
                .build();
    }

}

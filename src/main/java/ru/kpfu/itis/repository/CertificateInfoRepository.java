package ru.kpfu.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.entity.CertificateInfo;
import ru.kpfu.itis.entity.Graduate;
import ru.kpfu.itis.entity.WorkingPlace;
import ru.kpfu.itis.util.CertificateName;

import java.util.List;

@Repository
public interface CertificateInfoRepository extends JpaRepository<CertificateInfo,Long> {

    CertificateInfo save(CertificateInfo certificate);

    @Query(value = "SELECT c From certificate_info c WHERE c.graduate_id = :graduateId AND c.working_place_id = :workingPlaceId AND c.certificate_name = :certificateName", nativeQuery = true)
    CertificateInfo getCertificateInfoByGraduateAndWorkingPlaceAndCertificateName(@Param("graduateId") Integer graduateId, @Param("workingPlaceId") Integer workingPlaceId, @Param("certificateName") CertificateName certificateName);

    List<CertificateInfo> getCertificateInfoByGraduateAndWorkingPlace(Graduate graduate, WorkingPlace workingPlace);
}

package ru.kpfu.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.entity.PreviousEducationPlace;

@Repository
public interface PreviousEducationPlaceRepository extends JpaRepository<PreviousEducationPlace,Long> {

    PreviousEducationPlace save(PreviousEducationPlace previuosEduactionPlace);

    PreviousEducationPlace findByHighSchoolAndFaculty(String highSchool, String facultyName);
}

package ru.kpfu.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.entity.WorkingPlace;

import java.util.Date;
import java.util.List;

@Repository
public interface WorkingPlaceRepository extends JpaRepository<WorkingPlace, Long> {

       WorkingPlace save(WorkingPlace workingPlace);

       @Modifying
       @Query(value = "UPDATE working_place w SET is_last_working_place = false WHERE w.is_last_working_place = true AND w.graduate_id =:graduateId",nativeQuery = true)
       public void setNotLastWorkingPlaceForGraduate(@Param("graduateId") Integer graduateId);

       @Query(value = "SELECT w.end_date FROM working_place w WHERE w.is_last_working_place = true AND w.graduate_id =:graduateId",nativeQuery = true)
       public Date getEndDateOfLastWorkingPlace(@Param("graduateId") Integer graduateId);

       @Query(value = "SELECT organization_name,count(*) FROM working_place GROUP BY (inn,organization_name)", nativeQuery = true)
       public List<Object[]> getNumbersOfEmployeesByOrganizations();

       @Query(value = "SELECT organization_name,position,order_of_admission,(end_date - start_date) FROM working_place WHERE graduate_id =:graduateId", nativeQuery = true)
       public List<Object[]> getWorkingPlacesByStudent(@Param("graduateId") Integer graduateId);

       WorkingPlace getById(Integer id);

       @Query(value = "SELECT w FROM working_place w WHERE w.graduate_id =:graduateId AND w.is_last_working_place = :isLastWorkingPlace", nativeQuery = true)
       WorkingPlace findByGraduateIdAndIsLastWorkingPlace(@Param("graduateId") Integer graduateId, @Param("isLastWorkingPlace") boolean isLastWorkingPlace);

       @Query(value = "SELECT w FROM working_place w WHERE w.graduate_id =:graduateId", nativeQuery = true)
       List<WorkingPlace> getWorkingPlaceListByGraduateId(@Param("graduateId") Integer graduateId);

       @Modifying
       @Query(value = "UPDATE working_place w SET graduate_id =:graduateId WHERE w.id = :workingPlaceId",nativeQuery = true)
       public void setGraduateIdForWorkingPlace(@Param("graduateId") Integer graduateId, @Param("workingPlaceId") Integer workingPlaceId);

       @Query(value = "SELECT * FROM working_place w WHERE w.graduate_id = :graduateId", nativeQuery = true)
       public List<WorkingPlace> getWorkingPlaceListByGraduate(@Param("graduateId") Integer graduateId);


       @Modifying
       @Query(value = "UPDATE working_place w SET organization_name =:organizationName, " +
               "position =:position, start_date =:startDate, end_date =:endDate, " +
               "order_of_admission =:orderOfAdmission, " +
               "inn =:inn, kpp =:kpp, " +
               "medium_salary =:mediumSalary WHERE w.graduate_id =:graduateId AND w.is_last_working_place =: true", nativeQuery = true)
       public void editLastWorkingPlaceParams(@Param("graduateId") Integer graduateId,@Param("organizationName") String organizationName,
                                              @Param("position") String position, @Param("startDate") Date startDate,
                                              @Param("endDate") Date endDate,@Param("orderOfAdmission") String orderOfAdmission,
                                              @Param("inn") String inn, @Param("kpp") String kpp,
                                              @Param("mediumSalary") Double mediumSalary);


}

package ru.kpfu.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.entity.Region;

@Repository
public interface RegionRepository extends JpaRepository<Region,Integer> {

    Region getById(Integer id);

    Region getByRegionCode(Integer regionCode);
}

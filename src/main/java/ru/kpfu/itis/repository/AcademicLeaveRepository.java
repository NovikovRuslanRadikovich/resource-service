package ru.kpfu.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kpfu.itis.entity.AcademicLeave;

@Repository
public interface AcademicLeaveRepository extends JpaRepository<AcademicLeave,Long> {

    AcademicLeave findByLeavingYearAndComingYear(Integer leavingYear, Integer comingYear);

    AcademicLeave save(AcademicLeave academicLeave);
}

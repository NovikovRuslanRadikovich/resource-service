package ru.kpfu.itis.util;

public enum GrantValue {
    COMMERCIAL, EIGHTY_PERCENT, HUNDRED_PERCENT
}

package ru.kpfu.itis.util;

public class Constants {

    public static final Integer WORKED_YEARS = 4;
    public static final Integer MISSED_CERTIFICATE_SENDING_COUNT_OF_WORKED_GRADUATE = 0;
    public static final Integer MONTH_OF_GRADUATION = 6;
    public static final Integer DAY_OF_GRADUATION = 30;
}

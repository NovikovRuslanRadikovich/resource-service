package ru.kpfu.itis.util;

public class EmploymentError {

    public final static String GRADUATE_NOT_CREATED = "Graduate for current user is not created";

    public final static String END_DATE_OF_PREVIOUS_WORKING_PLACE_NOT_SPECIFIED = "You must specify the end date of previous working place before adding new working place";

    public final static String CERTIFICATE_FORMAT_IS_NOT_PDF_OR_DOCX = "Certificate of employment must be in pdf or docx format";

    public final static String IMAGE_NOT_UPLOADED = "Profile image wasn't uploaded";

    public final static String IMAGE_NOT_UNLOADED = "No image found for graduate";
}

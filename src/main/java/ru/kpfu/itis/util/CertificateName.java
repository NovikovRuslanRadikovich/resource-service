package ru.kpfu.itis.util;

public enum CertificateName {
    certificateOfEmploymentFile,
    certificateOfDeductions,
    certificateOfInsuranceDeductionsFromTaxService
}

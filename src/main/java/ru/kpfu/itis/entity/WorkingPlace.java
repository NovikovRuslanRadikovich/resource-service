package ru.kpfu.itis.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class WorkingPlace {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String organizationName;
    private String position;
    private Date startDate;
    private Date endDate;
    @Column(unique = true)
    private String inn;
    private String kpp;
    private String orderOfAdmission;
    private Double mediumSalary;
    private Boolean isLastWorkingPlace;
    @ManyToOne
    @JoinColumn(name = "graduate_id")
    private Graduate graduate;
}

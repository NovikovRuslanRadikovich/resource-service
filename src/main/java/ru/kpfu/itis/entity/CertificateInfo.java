package ru.kpfu.itis.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kpfu.itis.util.CertificateName;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class CertificateInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Enumerated
    private CertificateName certificateName;
    private String locationOfCertificate;
    @ManyToOne
    @JoinColumn(name = "graduate_id", referencedColumnName = "id")
    private Graduate graduate;
    @ManyToOne
    @JoinColumn(name = "workingPlace_id", referencedColumnName = "id")
    private WorkingPlace workingPlace;
}

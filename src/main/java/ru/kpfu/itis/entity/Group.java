package ru.kpfu.itis.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Date;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private int id;
    private String number;
    private Date yearOfCreation;
    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "graduatesGroups")
    private Set<Graduate> graduateSet;

}

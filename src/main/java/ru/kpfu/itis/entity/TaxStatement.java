package ru.kpfu.itis.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class TaxStatement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    @JoinColumn(name = "graduate_id", referencedColumnName = "id")
    private Graduate graduateByGraduateId;
    private String company;
    private String companyRegistrationPlace;
    private Timestamp taxStatementDate;
    private Double taxAmount;

}

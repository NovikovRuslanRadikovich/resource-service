package ru.kpfu.itis.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.kpfu.itis.util.GrantValue;

import javax.persistence.*;
import java.util.Set;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Grant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private Integer yearOfReceiving;

    @Enumerated(EnumType.STRING)
    private GrantValue grantValue;

    @Column
    private Integer courseNumber;

    @Column
    private Double grantAmount;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "graduatesGrants")
    private Set<Graduate> graduateSet;

}

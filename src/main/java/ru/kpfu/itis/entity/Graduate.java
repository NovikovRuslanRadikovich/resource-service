package ru.kpfu.itis.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Graduate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    private String name;
    private String surname;
    private String previousSurname;
    private String patronymic;
    private String phoneNumber;
    private Integer yearOfAdmission;
    private Integer yearOfGraduation;
    private Boolean hasPreviousEducationPlace;
    private Integer userId;
    private String profileImageLocation;
    @Column(name = "has_returned_money", nullable = false, columnDefinition = "boolean default false")
    private Boolean hasReturnedMoney;
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name="graduate_grant", joinColumns = {
            @JoinColumn(name = "graduate_id",nullable = true, updatable = false) },
            inverseJoinColumns = {@JoinColumn(name = "grant_id",
                    nullable = true,updatable = false)})
    private Set<Grant> graduatesGrants;
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name="graduate_group", joinColumns = {
            @JoinColumn(name = "graduate_id",nullable = true, updatable = false) },
            inverseJoinColumns = {@JoinColumn(name = "group_id",
                    nullable = true,updatable = false)})
    private Set<Group> graduatesGroups;
    @ManyToOne
    @JoinColumn(name = "region_id", referencedColumnName = "id")
    private Region region;
    @ManyToOne
    @JoinColumn(name = "academic_leave_id", referencedColumnName = "id")
    private AcademicLeave academicLeave;
    @ManyToOne
    @JoinColumn(name = "previous_education_place_id", referencedColumnName = "id")
    private PreviousEducationPlace previousEducationPlace;
    @OneToMany(mappedBy = "graduate")
    private List<WorkingPlace> workingPlaceList;

}

package ru.kpfu.itis.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class AcademicLeave {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Basic
    @Column(name = "leaving_year")
    private Integer leavingYear;
    @Basic
    @Column(name = "coming_year")
    private Integer comingYear;
    @OneToMany(mappedBy = "academicLeave")
    private Set<Graduate> graduateSet;


}
